from django.db import models
from datetime import datetime
# Create your models here.


class Status(models.Model):

    statusCondition = models.CharField(max_length=300)
    time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.statusCondition

