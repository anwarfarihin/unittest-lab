from .views import *
from django.urls import path

app_name = 'first_unittest'
urlpatterns = [
    path('', addStatus, name='addStatus'),
    path('lab8/', lab8, name='lab8'),
    path('lab9/', lab9, name='lab9'),
    path('lab9/api/books/', books, name='books'),
    path('home/', home, name='home'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
]
