$(function(){
    $("#accordion").accordion();
} );

$(function(){
    var bg = true;
    $("#change").click(
        function(){
            $("#imagebg").fadeIn("3000");
            if(bg){ //tadinya mau ngambil gambar dr static tp gatau cara ngeload tanpa dibikin satu folder gmn, jdya pek foto dr inet
                $("#imagebg").css({"backgroundImage" : "url(https://images.pexels.com/photos/956999/milky-way-starry-sky-night-sky-star-956999.jpeg?auto=compress&cs=tinysrgb&h=650&w=940%201x,%20https://images.pexels.com/photos/956999/milky-way-starry-sky-night-sky-star-956999.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940%202x)"});
                bg = false;
            } else{
                $("#imagebg").css({"backgroundImage" : "url(https://wallpapermemory.com//uploads/754/retro-wallpaper-full-hd-1920x1080-194947.jpg)"});
                bg = true;
            }
    });
});

var width = 100,
    perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page.
    EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
    time = parseInt((EstimatedTime/1000)%60)*100;

// Loadbar Animation
$(".loadbar").animate({
  width: width + "%"
}, time);

// Loadbar Glow Animation
$(".glow").animate({
  width: width + "%"
}, time);

// Percentage Increment Animation
var PercentageID = $("#precent"),
		start = 0,
		end = 100,
		durataion = time;
		animateValue(PercentageID, start, end, durataion);

function animateValue(id, start, end, duration) {

	var range = end - start,
      current = start,
      increment = end > start? 1 : -1,
      stepTime = Math.abs(Math.floor(duration / range)),
      obj = $(id);

	var timer = setInterval(function() {
		current += increment;
		$(obj).text("loading " + current + "%");
      //obj.innerHTML = current;
		if (current == end) {
			clearInterval(timer);
		}
	}, stepTime);
}

// Fading Out Loadbar on Finised
setTimeout(function(){
  $('.preloader-wrap').fadeOut(1000);
}, time);



