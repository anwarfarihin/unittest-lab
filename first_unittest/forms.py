from django import forms
from .models import Status


class StatusForm(forms.ModelForm):
    attrs_status = {
        'class': 'form-control',
        'size': 45,
        'placeholder': 'Masukkan status Anda'
    }

    statusCondition = forms.CharField(label='Kondisi', required=True,
                                      widget=forms.widgets.TextInput(attrs=attrs_status))

    class Meta:
        model = Status
        fields = ['statusCondition']


