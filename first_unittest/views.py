from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
from django.http import JsonResponse
import requests


# Create your views here.

def addStatus(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    else:
        form = StatusForm()
        status = Status.objects.all().order_by("time")[::-1]
        return render(request, 'index.html', {'form': form, 'status': status, })


def lab8(request):
    return render(request, 'lab8.html')


def lab9(request, search="quilting"):
    return render(request, 'lab9.html', {'search': search})


def books(request):
    raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting').json()
    items_ = []
    for info in raw_data['items']:
        data = {}
        data['image'] = info['volumeInfo']['imageLinks']['thumbnail']
        data['title'] = info['volumeInfo']['title']
        data['author'] = info['volumeInfo']['authors'][0]
        data['publishedDate'] = info['volumeInfo']['publishedDate']
        items_.append(data)

    return JsonResponse({"data": items_})


def home(request):
    return render(request, 'lab9.html')

def login(request):
    return render(request, 'login.html')


def logout(request):
    logout(request)
    return redirect('login')
