from django.test import TestCase, Client
from django.urls import resolve
from .views import addStatus
from django.http import HttpRequest
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest




class Lab6Unittest(TestCase):
    def test_lab_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_lab_6_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, addStatus)

    def test_landing_page_is_completed(self):
        request = HttpRequest()
        response = addStatus(request)
        html_response = response.content.decode('utf8')
        self.assertIn('SUP BRO?', html_response)

    def test_lab6_using_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_model_can_create_status(self):
        Status.objects.create(statusCondition='pushinnngg')
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'status': '', 'date': ''})
        self.assertFalse(form.is_valid())

class Lab6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        self.browser.get('http://tdd102.herokuapp.com/')
        input_box = self.browser.find_element_by_name('statusCondition')
        input_box.send_keys('Coba Coba')
        input_box.submit()
        self.assertIn('Coba Coba', self.browser.page_source)


    # def test_h1_is_using_century_gothic(self):
    #     self.browser.get('http://tdd102.herokuapp.com/')
    #     h1font = self.browser.find_element_by_tag_name('h1').value_of_css_property('font-family')
    #     self.assertEqual('"Century Gothic"', h1font)

    def test_is_title_match(self):
        self.browser.get('http://tdd102.herokuapp.com/')
        self.assertIn("Anwar Farihin", self.browser.title)

    def test_lab8_is_title_match(self):
        self.browser.get('http://tdd102.herokuapp.com/lab8')
        self.assertIn("Anwar Farihin", self.browser.title)

    def test_lab9_is_title_match(self):
        self.browser.get('http://tdd102.herokuapp.com/lab9')
        self.assertIn("lab 9 farihin", self.browser.title)

    # def test_body_with_css(self):
    #     self.browser.get('http://tdd102.herokuapp.com/')
    #     body = self.browser.find_element_by_tag_name('body')
    #     background_image = body.value_of_css_property("background-color")
    #     height = body.value_of_css_property('height')
    #     self.assertLessEqual('2500px', height)
    #     self.assertEqual("rgba(8, 32, 56, 1)", background_image)
    #
    # def test_submit_button_position(self):
    #     self.browser.get('http://tdd102.herokuapp.com/')
    #     button = self.browser.find_element_by_tag_name('button')
    #     self.assertGreaterEqual(button.location['x'],363)
    #     self.assertLessEqual(button.location['y'],121)

    def test_lab9_layout_header_with_css_property(self):
        browser = self.browser
        browser.get('http://tdd102.herokuapp.com/lab9')
        content = browser.find_element_by_tag_name('h2').value_of_css_property('color')
        self.assertIn('rgba(255, 255, 255, 1)', content)


if __name__ == '__main__':  #
    unittest.main(warnings='ignore')  #

