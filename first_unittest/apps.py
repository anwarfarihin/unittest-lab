from django.apps import AppConfig


class FirstUnittestConfig(AppConfig):
    name = 'first_unittest'
