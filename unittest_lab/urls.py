"""unittest_lab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from first_unittest import urls as first_unittest_urls
from lab10 import urls as lab10_urls

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('auth/', include('social_django.urls', namespace='social')),  # <- Here
                  path('lab10/', include(lab10_urls), name="lab10"),
                  path('', include(first_unittest_urls), name="index")] + \
              static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
