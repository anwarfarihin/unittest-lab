var counter =0;

$(function(){
var search= $('.search').attr("id");
console.log(search);
    $.ajax({
        type : 'GET',
        url: "/lab9/api/books/"+search,
        dataType: 'json',
        success: function(data){
            var temp = '';
            console.log("ea")
            for(var i = 0;i<data.data.length;i++){
                temp+='<tr class="text-center"><th scope="row">' + (i+1) + '</th>';
                temp+='<td class="align-middle"> <img class="img-responsive" src="' + data.data[i].image+'"></td>';
                temp+='<td class="align-middle"><b>' + data.data[i].title+'</b></td>';
                temp+='<td class="align-middle">'+ data.data[i].author +'</td>';
                temp+='<td class="align-middle">'+ data.data[i].publishedDate +'</td>';
                temp+='<td class="align-middle"><i class="fas fa-star" id="star" style="font-size:30px;"></i></td>';
                temp+='</tr>';
            }
            $('tbody').append(temp);
        }
    });
    $(document).on('click', '#star', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            console.log(counter);
            $(this).removeClass('clicked');
            $(this).css("color","rgba(150,150,150,0.5);");
        } else {
            counter +=1;
            console.log(counter);
            $(this).addClass('clicked');
            $(this).css("color","#FFD300");
        }
        $('.count').html(counter);
    });
});