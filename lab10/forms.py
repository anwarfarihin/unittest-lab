from django import forms

class SubscriberForm(forms.Form):
    attrs_password = {
        'class': 'form-control',
        'id': 'pass-form',
        'placeholder': 'Password',
    }
    attrs_email = {
        'class': 'form-control',
        'id': 'email-form',
        'placeholder': 'email@example.com'
    }
    attrs_name = {
        'class': 'form-control',
        'id': 'name-form',
        'placeholder': 'Your Name'

    }
    name = forms.CharField(max_length=50, widget=forms.TextInput(attrs=attrs_name), label='Name')
    email = forms.EmailField(widget=forms.EmailInput(attrs=attrs_email), label='Email')
    password = forms.CharField(max_length=50, min_length=8,widget=forms.PasswordInput(attrs=attrs_password), label='Password')
