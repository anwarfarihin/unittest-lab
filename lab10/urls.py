from . import views
from django.urls import path

app_name = 'lab10'
urlpatterns = [
    path('subscribe/', views.subscribe, name='subscribe'),
    path('validate', views.validate, name='validate'),
    path('getSubscriber/', views.get_subscriber),
    path('delSubscriber/', views.del_subscriber),
    path('', views.lab10, name='lab_10'),
]
