from django.shortcuts import render, redirect
from .models import Subscriber
from .forms import SubscriberForm
import requests
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
import json
from django.core import serializers


# Create your views here.
response={}
def lab10(request):
    form = SubscriberForm(request.POST or None)
    subscribers_set = Subscriber.objects.all()
    return render(request, "lab10.html", {'form': form,'subscribers': subscribers_set})


@csrf_exempt
def subscribe(request):
    if request.method == 'POST':
        enctype = "multipart/form-data"
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        subscribers = Subscriber(name=name, email=email, password=password)
        subscribers.save()

        data = serializers.serialize('json', [subscribers, ])
        struct = json.loads(data)
        data = json.dumps(struct[0]["fields"])

        return HttpResponse(data)
    else:
        subscribers_set = Subscriber.objects.all().values()
        data = list(subscribers_set)
        return JsonResponse(data, safe=False)


@csrf_exempt
def validate(request):
    enctype = "multipart/form-data"
    email = request.POST.get('email')
    data = {
        'is_taken': Subscriber.objects.filter(email=email).exists()
    }
    return JsonResponse(data)

def get_subscriber(request):
    if request.method == 'GET':
        lst_subscriber = serializers.serialize('json', Subscriber.objects.all())
        response = lst_subscriber
    else:
        response['message'] = 'Method POST not ALLOWED HERE'
    return HttpResponse(response, content_type='application/json')

def del_subscriber(request):
    if request.method == 'POST':
        subscriber = Subscriber.objects.get(pk=request.POST.get('pk'))
        print(subscriber)
        subscriber.delete()
        response['message'] = "success"
    else:
        response['message'] = "Method GET not allowed here"
    return JsonResponse(response)


